
## node.js scraper for medium
Write a scraper in node.js that will fetch all internal urls from all pages of medium.com (recursively) and save them into a file. The number of concurrent connection should be maintained to 5 at any given point in time. 

### Instructions
* You are not supposed to use any external scraping library.
* You are not supposed to use any external connection management library.
* Maintain your code on bitbucket along with proper readme files containing all instructions to setup and run the code
* Follow standard coding practices of the language.

const kafka = require('kafka-node');
var KafkaConfig = require('../config/kafka')
        
module.exports = {
  uploadData : function(data){
    const Producer = kafka.Producer
    const KClient = new kafka.Client(KafkaConfig.kafka_server,'producer-client',{ sessionTimeout: 100, spinDelay: 100, retries: 2 })
    const producer = new Producer(KClient,{ requireAcks: 1 });
    console.log('==============================OHHH===============================')
    var payloads = [
      {
        topic: "producttesting1",
        attributes: 2,
        partition : 0,
        messages: JSON.stringify({
          msg: 'test22',
          timestamp: Date.now()
        })

      }
    ];
    console.log(payloads)
    producer.on('ready', function() {
      console.log('==============================WENT===============================')
      let push_status = producer.send(payloads, (err, data) => {
        console.log('============================Reaching===============================')
        console.log(data)
        if (err) {
          console.log('[kafka-producer -> '+''+']: broker update failed');
        } else {
          console.log('[kafka-producer -> '+''+']: broker update success');
        }
      });
    });

    producer.on('error', function(err) {
      console.log(err);
      console.log('[kafka-producer -> '+''+']: connection errored');
      throw err;
    });
  }
}